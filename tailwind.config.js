module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        "ms-purple": "#AC27F4",
        "ms-blue": "#273AE9",
        "ms-izzi": "#5920BC",
        "ms-school": "#6537A6",
        "ms-orange": "#EF794A",
        "ms-light-orange": "#F7B142",
        "ms-black": "#121315"
      },
      fontFamily: {
        // russoOne: "'Russo One'",
        averta: ["Averta", "sans-serif"]
      },
      container: {
        center: true
      },
    },
  },
  plugins: [],
}
